package dam.androidAlejandro.u2p4conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends LogActivity{

    public MainActivity() {
        super.setDEBUG_TAG(this.getClass().getSimpleName());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        EditText etPulgada = findViewById(R.id.et_Pulgada);
        EditText etCm = findViewById(R.id.et_Resultado);
        Button buttonConvertir = findViewById(R.id.button_Convertir);
        Button buttonConvertirCm = findViewById(R.id.button_ConvertirCm_pulgadas);
        TextView textViewEx = findViewById(R.id.textViewEx);

        buttonConvertir.setOnClickListener(view -> {
            // TODO: Ex3: Añadido información sobre botones pulsados en ambos botones.
            Log.i(super.getDEBUG_TAG(),"Botón convertir a cm pulsado");
            try {
                etCm.setText(convertir(etPulgada.getText().toString()));
            } catch (Exception e) {
                if (!e.getMessage().equals("empty String")){
                    //TODO: Ex2: Mostrado por textView el error de la excepción Solo números >= 1.
                    textViewEx.setText(e.getMessage());
                }
            }
        });
        // TODO: Ex1: Conversión en ambos sentidos
        buttonConvertirCm.setOnClickListener(view -> {
            Log.i(super.getDEBUG_TAG(),"Botón convertir a pulgadas pulsado");
            try {
                etPulgada.setText(convertirPg(etCm.getText().toString()));
            } catch (Exception e) {
                if (!e.getMessage().equals("empty String")){
                    textViewEx.setText(e.getMessage());
                }
            }
        });
    }

    //TODO: Ex2: Restringido que se pueda añadir un valor < 1 en ambos metodos.
    private String convertir(String pulgadaText) throws Exception {
        double pulgadaValue = Double.parseDouble(pulgadaText);
        if (pulgadaValue < 1) {
                throw new Exception("Solo números >= 1");
        } else {
            pulgadaValue *= 2.54;
            return String.valueOf(pulgadaValue);
        }
    }

    private String convertirPg(String pulgadaText) throws Exception {
        double pulgadaValue = Double.parseDouble(pulgadaText);
        if (pulgadaValue < 1) {
            throw new Exception("Solo números >= 1");
        } else {
            pulgadaValue /= 2.54;
            return String.valueOf(pulgadaValue);
        }
    }
}