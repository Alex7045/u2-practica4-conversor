package dam.androidAlejandro.u2p4conversor;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class LogActivity extends AppCompatActivity {
    private String DEBUG_TAG = "Log-";

    public String getDEBUG_TAG() {
        return DEBUG_TAG;
    }

    private void notify(String eventName){
        String activityName = this.getClass().getSimpleName();

        String CHANNEL_ID = "My_LifeCycle";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID,"My Lifecycle",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);

            if (notificationManager != null){
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }

        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName+" "+activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int)System.currentTimeMillis(),notificationBuilder.build());
    }

    @Override
    // TODO: Ex4 corregido el problema de que se borren los strings cuando volteamos
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        TextView textViewEx = findViewById(R.id.textViewEx);
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            String texto = savedInstanceState.getString("");
            textViewEx.setText(texto);
        }
        Log.i(DEBUG_TAG,"Restaurado el estado anterior");
    }



    @Override
    // TODO: Ex4 corregido el problema de que se borren los strings cuando volteamos
    protected void onSaveInstanceState(Bundle outState) {
        TextView textViewEx = findViewById(R.id.textViewEx);
        super.onSaveInstanceState(outState);
        outState.putString("",textViewEx.getText().toString());
        Log.i(DEBUG_TAG,"Guardado el estado anterior");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(DEBUG_TAG,"onCreate");
        notify("onCreate");
    }

    public void setDEBUG_TAG(String DEBUG_TAG) {
        this.DEBUG_TAG += DEBUG_TAG;
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG,"onPause");
        notify("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG,"onResume");
        notify("on Resume");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(DEBUG_TAG,"onStart");
        notify("onStart");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG,"onStop");
        notify("onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG,"onRestart");
        notify("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(DEBUG_TAG,"onDestroy");
        notify("onDestroy");
        if (isFinishing()){
            Log.i(DEBUG_TAG,"Finalizado por el usuario");
        }else{
            Log.i(DEBUG_TAG,"Finalizado por la aplicación");
        }
    }
}
